/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.util.Date;
public class NhanVien
{
    private String MaNV;
    private String Ten;
    private String DiaChi;
    private Date NgaySinh;
    private boolean GioiTinh;
    
    public NhanVien()
    {
    }
        public String getMaNV()
        {
            return MaNV;
        }
        public void setMaNV(String MaNV)
        {
            this.MaNV=MaNV;
        }
        public String getTen()
        {
            return Ten;
        }
        public void setTen(String Ten)
        {
            this.Ten=Ten;
        }
        public String getDiaChi()
        {
            return DiaChi;
        }
        public void setDiaChi(String DiaChi)
        {
            this.DiaChi=DiaChi;
        }
        public Date getNgaySinh()
        {
            return NgaySinh;
        }
        public void setNgaySinh(Date NgaySinh)
        {
            this.NgaySinh=NgaySinh;
        }
        public boolean getGioiTinh()
        {
            return GioiTinh;
        }
        public void setGioiTinh(boolean GioiTinh)
        {
            this.GioiTinh=GioiTinh;
        }
                
}
    

