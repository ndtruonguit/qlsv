/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author WIN
 */
public class QLTT extends javax.swing.JFrame 
{
    NhanVienDAO nvDAO = new NhanVienDAO();
    SimpleDateFormat d = new SimpleDateFormat("dd/mm/yyyy");
    public QLTT()
    {  
        initComponents();
        setLocationRelativeTo(null);   
    }
    public void Reset()
    {
        txtManv.setText("");
        txtTen.setText("");
        txtDiaChi.setText("");
        txtNgaySinh.setText("");
        rdbNam.setSelected(true);
        rdbNu.setSelected(false);  
    }
    public boolean CheckForm()
    {
        return !(txtManv.getText().isEmpty()||txtTen.getText().isEmpty()||txtDiaChi.getText().isEmpty()||txtNgaySinh.getText().isEmpty());
    }
    public NhanVien Nhap()throws ParseException
    {
        NhanVien nv=new NhanVien();
        nv.setMaNV(txtManv.getText());
        nv.setTen(txtTen.getText());
        nv.setDiaChi(txtDiaChi.getText());
        nv.setNgaySinh(d.parse(txtNgaySinh.getText()));
        boolean gt=false;if(rdbNam.isSelected()) gt=true;nv.setGioiTinh(gt);
        return nv;   
    }
    public void Xuat(NhanVien nv)
    {
        txtManv.setText(nv.getMaNV());
        txtTen.setText(nv.getTen());
        txtDiaChi.setText(nv.getDiaChi());
        txtNgaySinh.setText(d.format(nv.getNgaySinh()));
        rdbNam.setSelected(nv.getGioiTinh());
        rdbNu.setSelected(nv.getGioiTinh());
    }
    
    

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        maNV = new javax.swing.JLabel();
        hoten = new javax.swing.JLabel();
        ngaysinh = new javax.swing.JLabel();
        gioitinh = new javax.swing.JLabel();
        diachi = new javax.swing.JLabel();
        tongso = new javax.swing.JLabel();
        txtManv = new javax.swing.JTextField();
        txtTen = new javax.swing.JTextField();
        txtNgaySinh = new javax.swing.JTextField();
        rdbNam = new javax.swing.JRadioButton();
        rdbNu = new javax.swing.JRadioButton();
        txtDiaChi = new javax.swing.JTextField();
        btnTim = new javax.swing.JButton();
        btnSua = new javax.swing.JButton();
        btnthem = new javax.swing.JButton();
        btnLuu = new javax.swing.JButton();
        thoat = new javax.swing.JButton();
        btnXoa = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("THÔNG TIN");

        maNV.setText("MaNV");

        hoten.setText("Ho Ten");

        ngaysinh.setText("Ngày Sinh");

        gioitinh.setText("Giới Tính");

        diachi.setText("Địa Chỉ");

        tongso.setText("Tổng số");

        rdbNam.setText("Nam");

        rdbNu.setText("Nữ");

        btnTim.setText("Tìm");
        btnTim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTimActionPerformed(evt);
            }
        });

        btnSua.setText("Sửa");
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });

        btnthem.setText("Thêm");
        btnthem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnthemActionPerformed(evt);
            }
        });

        btnLuu.setText("Lưu");
        btnLuu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLuuActionPerformed(evt);
            }
        });

        thoat.setText("Thoát");
        thoat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                thoatActionPerformed(evt);
            }
        });

        btnXoa.setText("Xóa");
        btnXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(maNV)
                            .addComponent(hoten)
                            .addComponent(ngaysinh)
                            .addComponent(gioitinh)
                            .addComponent(diachi))
                        .addGap(54, 54, 54)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(rdbNam)
                            .addComponent(txtManv)
                            .addComponent(txtTen)
                            .addComponent(txtNgaySinh)
                            .addComponent(txtDiaChi, javax.swing.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tongso)
                        .addGap(207, 207, 207)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rdbNu)
                            .addComponent(jLabel1))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(thoat)
                        .addGap(109, 109, 109))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(188, 188, 188)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnTim)
                            .addComponent(btnthem)
                            .addComponent(btnLuu)
                            .addComponent(btnSua)
                            .addComponent(btnXoa))
                        .addContainerGap(127, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel1)
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(maNV)
                    .addComponent(txtManv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTim))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hoten)
                            .addComponent(txtTen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(btnthem)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ngaysinh)
                            .addComponent(txtNgaySinh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(67, 67, 67)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(gioitinh)
                            .addComponent(rdbNam)
                            .addComponent(rdbNu))
                        .addGap(75, 75, 75)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(diachi)
                            .addComponent(txtDiaChi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(81, 81, 81)
                        .addComponent(tongso))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addComponent(btnLuu)
                        .addGap(64, 64, 64)
                        .addComponent(btnSua)
                        .addGap(104, 104, 104)
                        .addComponent(btnXoa)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addComponent(thoat)
                .addGap(77, 77, 77))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void thoatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_thoatActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_thoatActionPerformed

    private void btnTimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTimActionPerformed
        // TODO add your handling code here:
        NhanVien nvTim=nvDAO.Tim(txtManv.getText());
        if (nvTim!=null) {
            Xuat(nvTim);
        } else {
            JOptionPane.showMessageDialog(this,"Khong tim thay");
        }
    }//GEN-LAST:event_btnTimActionPerformed

    private void btnthemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnthemActionPerformed
        // TODO add your handling code here:
        Reset();
    }//GEN-LAST:event_btnthemActionPerformed

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        // TODO add your handling code here:
        try
        {
            NhanVien nv=Nhap();
            if(nvDAO.Sua(nv)>0) JOptionPane.showMessageDialog(this,"Sua thanh cong");
            else JOptionPane.showMessageDialog(this,"Chua sua duoc");
        }
        catch(Exception e)
        {
            
        }
    }//GEN-LAST:event_btnSuaActionPerformed

    private void btnLuuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLuuActionPerformed
        // TODO add your handling code here:
        if(CheckForm())
        {
            NhanVien nv;
            try {
                nv=Nhap();
                if(nvDAO.Them(nv)>0)
                {
                    JOptionPane.showMessageDialog(this,"Them thanh cong");
                    tongso.setText("Tong So:" + nvDAO.Dem());
                }
                else
                    JOptionPane.showMessageDialog(this,"Chua nhap du thong tin");
            } catch(Exception e){
                    
            }
        }
    }//GEN-LAST:event_btnLuuActionPerformed

    private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaActionPerformed
        // TODO add your handling code here:
        if(txtManv.getText().isEmpty())
        {
            JOptionPane.showMessageDialog(this,"Ban chua nhap du ma de xoa");
        }
        else
        {
            if(nvDAO.Xoa(txtManv.getText())>0)
            {
                JOptionPane.showMessageDialog(this,"Xoa thanh cong");
                tongso.setText("Tong So:" + nvDAO.Dem());
                Reset();
            }
            else JOptionPane.showMessageDialog(this,"Khong tim thay");
        }
    }//GEN-LAST:event_btnXoaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(QLTT.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new QLTT().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLuu;
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnTim;
    private javax.swing.JButton btnXoa;
    private javax.swing.JButton btnthem;
    private javax.swing.JLabel diachi;
    private javax.swing.JLabel gioitinh;
    private javax.swing.JLabel hoten;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel maNV;
    private javax.swing.JLabel ngaysinh;
    private javax.swing.JRadioButton rdbNam;
    private javax.swing.JRadioButton rdbNu;
    private javax.swing.JButton thoat;
    private javax.swing.JLabel tongso;
    private javax.swing.JTextField txtDiaChi;
    private javax.swing.JTextField txtManv;
    private javax.swing.JTextField txtNgaySinh;
    private javax.swing.JTextField txtTen;
    // End of variables declaration//GEN-END:variables
}
