/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author WIN
 */
import java.util.ArrayList;
public class NhanVienDAO
{
    ArrayList<NhanVien>list=new ArrayList<>();
    public int Them(NhanVien e)
    {
        list.add(e);
        return 1;
    }
    public NhanVien Tim(String maNV)
    {
        for(NhanVien nv:list)
        {
            if(nv.getMaNV().equalsIgnoreCase(maNV))
            {
                return nv;
            }
        }
        return null;
    }
    public int Sua(NhanVien e)
    {
        for(NhanVien nv:list)
        {
            if(nv.getMaNV().equalsIgnoreCase(e.getMaNV()))
            {
                nv.setTen(e.getTen());
                nv.setDiaChi(e.getDiaChi());
                nv.setGioiTinh(e.getGioiTinh());
                nv.setNgaySinh(e.getNgaySinh());
                return 1;
            }   
        }
        return-1;
    }
    
        public int Xoa(String manv)
        {
            NhanVien e=Tim(manv);
            if(e!=null)
            {
                list.remove(e);
                return 1;
            }
            return-1;
        }
        public int Dem()
        {
            return list.size();
        }
 }

